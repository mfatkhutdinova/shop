module git.ozon.dev/mfatkhutdinova/shop

go 1.14

require (
	git.ozon.dev/mfatkhutdinova/shop/pkg/shop-service v0.0.0-00010101000000-000000000000
	github.com/jackc/pgx/v4 v4.8.1
	github.com/jmoiron/sqlx v1.2.0
	github.com/pkg/errors v0.9.1
	github.com/utrack/clay/v2 v2.4.9
	gitlab.ozon.ru/platform/healthcheck-go v0.6.0
	gitlab.ozon.ru/platform/realtime-config-go v1.8.9
	gitlab.ozon.ru/platform/scratch v1.6.12-goschool
	gitlab.ozon.ru/platform/tracer-go v1.19.0
)

replace git.ozon.dev/mfatkhutdinova/shop/pkg/shop-service => ./pkg/shop-service
