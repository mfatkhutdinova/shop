module git.ozon.dev/mfatkhutdinova/shop/pkg/shop-service

go 1.14

require (
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/go-openapi/spec v0.19.9
	github.com/golang/protobuf v1.4.2
	github.com/grpc-ecosystem/grpc-gateway v1.14.6
	github.com/pkg/errors v0.9.1
	github.com/utrack/clay/v2 v2.4.9
	google.golang.org/genproto v0.0.0-20200731012542-8145dea6a485
	google.golang.org/grpc v1.29.1
)

