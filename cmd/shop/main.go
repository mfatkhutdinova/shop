package main

import (
	"context"
	shop_service "git.ozon.dev/mfatkhutdinova/shop/internal/app/shop-service"
	"git.ozon.dev/mfatkhutdinova/shop/internal/config"
	_ "git.ozon.dev/mfatkhutdinova/shop/internal/config"
	"git.ozon.dev/mfatkhutdinova/shop/internal/pkg/healthcheck"
	"git.ozon.dev/mfatkhutdinova/shop/internal/pkg/prepare"

	hc "gitlab.ozon.ru/platform/healthcheck-go"
	"gitlab.ozon.ru/platform/scratch"
	_ "gitlab.ozon.ru/platform/scratch/app/pflag"
	"gitlab.ozon.ru/platform/tracer-go/logger"
)

func main() {
	a, err := scratch.New()
	if err != nil {
		logger.Fatalf(context.Background(), "can't create app: %s", err)
	}

	impl := shop_service.NewShopService()

	go healthchecks(a.Healthcheck(), impl)
	go preparation(impl)

	if err := a.Run(impl); err != nil {
		logger.Fatalf(context.Background(), "can't run app: %s", err)
	}
}

func healthchecks(handler hc.Handler, impl *shop_service.Implementation) {
	healthcheck.Database(handler, impl)
}

func preparation(impl *shop_service.Implementation) {
	dsn := config.GetValue(context.TODO(), config.DbDsn).String()
	if err := prepare.Database(impl, dsn); err != nil {
		panic("db prepare panic:" + err.Error())
	}
}

