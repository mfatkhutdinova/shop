package prepare

import (
	// pgx db driver
	_ "github.com/jackc/pgx/v4/stdlib"
	"github.com/jmoiron/sqlx"
)

// DBSetter *sqlx.DB inject interface
type DBSetter interface {
	SetDB(db *sqlx.DB)
}

// Database inject database to impl
func Database(impl DBSetter, dsn string) error {
	db, err := sqlx.Connect("pgx", dsn)
	if err != nil {
		return err
	}
	impl.SetDB(db)
	return nil
}
