package healthcheck

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	hc "gitlab.ozon.ru/platform/healthcheck-go"
)

// DBGetter retrieve interface for *sqlx.DB
type DBGetter interface {
	GetDB() *sqlx.DB
}

// Database init database healthcheck
func Database(handler hc.Handler, impl DBGetter) {
	handler.AddReadinessCheck("db", func() error {
		db := impl.GetDB()
		if db == nil {
			return fmt.Errorf("db is nil")
		}

		if err := db.Ping(); err != nil {
			return fmt.Errorf("db ping error: %s", err)
		}

		return nil
	})
}
